widget: {
    # nice window, han?
    window: {
        color_hex: #0000FF,
        color_rgb: [0 0 255],
        height: 200,
        name: My Test Widget,
        width: 200,
    },
    image: {
        src: images/background.png,
        transform: { height: 150, width: 150, x: 25, y: 25, },
        visible: true,
    },
}